# Sheepdog Check_MK local plugins

Local checks to keep an eye on sheepdog storage cluster.

## Installation

* Create a config file `/etc/check_mk/sheepdog` with variables describing your cluster (see sample config and each check)
* Copy scripts to `/usr/lib/check_mk_agent/local/` or a [cached directory](https://mathias-kettner.de/checkmk_localchecks.html) (see chapter 6.)

## Checks

### `sheepdog_node_list`

This check just counts the cluster nodes and checks against the following variables in config:

* `NUMBER_OF_DOG_NODES_WARN`: threshold for warning (if less or equal)
* `NUMBER_OF_DOG_NODES_CRIT`: threshold for critical (if less or equal)
* `CLUSTER_SIZE`: desired amount of cluster nodes

### `sheepdog_node_recovery`

This check counts the amount of nodes currently recovering data.

* `NUMBER_OF_DOG_NODES_IN_RECOVERY_WARN`: threshold for warning (if less or equal)
* `NUMBER_OF_DOG_NODES_IN_RECOVERY_CRIT`: threshold for critical (if less or equal)

### `sheepdog_node_usage`

This check checks the cluster disk usage on each node and in total

* `USED_WARN`: threshold for warning (if equal or more)
* `USED_CRIT`: threshold for critical (if equal or more)
